'use strict'
var request = require('request')
var cheerio = require('cheerio')
var moment = require('moment')
var mongoose = require('mongoose')

var source = 'winter olympics 2018'
var url = 'https://wintergames.ap.org/aroundosceola/medals'

module.exports.crawler = (event, context, callback) => {
  request.get(url,
    function (error, response, body) {
      if (error) {
        callback(null, error)
      }
      if (response.statusCode >= 400) {
        console.log('response.statusCode: ', response.statusCode)
        callback(null, 'response.statusCode: ' + response.statusCode)
      }
      // console.log('body: ', body)
      const $ = cheerio.load(body)
      var thResult = $('th').map(function (i, el) {
        // this === el
        return $(this).text()
      }).get().join(',')
      // console.log(JSON.stringify(thResult, null, 2))
      var thArray = thResult.split(',').slice(6)
      // console.log(JSON.stringify(thArray, null, 2))
      var resultArray = []
      var counter = 0
      var team = {}
      for (let index = 0; index < thArray.length; index++) {
        switch (counter) {
          case 0:
            team.rank = thArray[index]
            counter += 1
            break
          case 1:
            team.name = thArray[index]
            counter += 1
            break
          case 2:
            team.gold = thArray[index]
            counter += 1
            break
          case 3:
            team.sliver = thArray[index]
            counter += 1
            break
          case 4:
            team.bronze = thArray[index]
            counter += 1
            break
          case 5:
            team.total = thArray[index]
            counter += 1
            break
          case 6:
            // console.log(JSON.stringify(team, null, 2))
            resultArray.push(team)
            team = {}
            counter = 0
            break
          default:
            break
        }
      }
      // console.log(JSON.stringify(resultArray, null, 2))
      mongoose.Promise = global.Promise
      mongoose.connect(
        'mongodb://mongos1b.mldev.twappledaily.com/motherlode'
      )
      var model = require('./model.js')

      model.MLSportRankInfo.findOne({
        'source': source
      }, function (error, queryExist) {
        if (error) {
          callback(null, error)
        }
        // console.log(JSON.stringify(queryExist, null, 2))
        if (queryExist == null) {
          var newSummary = new model.MLSportRankInfo({
            'source': source,
            'updateTime': moment().add(8, 'hours'),
            'url': url,
            'content': resultArray
          })
          newSummary.save(function (error, insertResult) {
            if (error) {
              callback(null, error)
            }
            callback(null, insertResult)
          })
        } else {
          model.MLSportRankInfo.findOneAndUpdate({
            'source': source
          }, {
            $set: {
              'updateTime': moment().add(8, 'hours'),
              'content': resultArray
            }
          }, {
            new: true
          }, function (error, updateResult) {
            if (error) {
              callback(null, error)
            }
            callback(null, updateResult)
          })
        }
      })
    })
}
