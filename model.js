var mongoose = require('mongoose')

const MLSportRankInfoSchema = mongoose.Schema({
  updateTime: { type: Date },
  source: { type: String },
  url: { type: String },
  content: [{
    rank: { type: String },
    name: { type: String },
    gold: { type: String },
    sliver: { type: String },
    bronze: { type: String },
    total: { type: String }
  }]
}, { collection: 'MLSportRankInfo' })
const MLSportRankInfo = mongoose.model('MLSportRankInfo', MLSportRankInfoSchema)
module.exports = MLSportRankInfo

module.exports = {
  MLSportRankInfo: MLSportRankInfo
}
