'use strict'
var request = require('request')
var cheerio = require('cheerio')
const AWS = require('aws-sdk')
const dynamo = new AWS.DynamoDB.DocumentClient()

module.exports.crawler = (event, context, callback) => {
  request.get('https://wintergames.ap.org/aroundosceola/medals',
    function (error, response, body) {
      if (error) {
        console.log('error: ', error)
        return
      }
      if (response.statusCode >= 400) {
        console.log('response.statusCode: ', response.statusCode)
        return
      }
      // console.log('body: ', body)
      const $ = cheerio.load(body)
      var thResult = $('th').map(function (i, el) {
        // this === el
        return $(this).text()
      }).get().join(',')
      // console.log(JSON.stringify(thResult, null, 2))
      var thArray = thResult.split(',').slice(6)
      // console.log(JSON.stringify(thArray, null, 2))
      var resultArray = []
      var counter = 0
      var team = {}
      for (let index = 0; index < thArray.length; index++) {
        switch (counter) {
          case 0:
            team.rank = thArray[index]
            counter += 1
            break
          case 1:
            team.name = thArray[index]
            counter += 1
            break
          case 2:
            team.gold = thArray[index]
            counter += 1
            break
          case 3:
            team.sliver = thArray[index]
            counter += 1
            break
          case 4:
            team.bronze = thArray[index]
            counter += 1
            break
          case 5:
            team.total = thArray[index]
            counter += 1
            break
          case 6:
            // console.log(JSON.stringify(team, null, 2))
            resultArray.push(team)
            team = {}
            counter = 0
            break
          default:
            break
        }
      }
      console.log(JSON.stringify(resultArray, null, 2))
      // callback(null, resultArray)
      dynamo.put({
        'TableName': 'medals',
        'ReturnConsumedCapacity': 'TOTAL',
        'Item': {
          'listingId': new Date().toString(),
          'summary': resultArray
        }
      }, function (err, putResult) {
        if (err) {
          return err;
        }
        console.log('Dynamo Success: ' + JSON.stringify(putResult, null, '  '));
        context.succeed('SUCCESS');
        })
    })
}